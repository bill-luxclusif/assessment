
Three Points: Origin, Route and Destinations

- many to many relationship between origin and route, and destination and route ( db table: 'route_in_origin' and 'route_in_destination' )
- cost and time are assigned and fixed between routes and origin or destination
- in the case that a route must go through another route to reach the destination, a fixed cost of $150 will be charged for each route and 60 minutes of time


- there are no validations so data duplication is possible
- no user accounts as well as authentication (needs further knowledge for api auth :D)
- use "PATCH" for updates


********** Origin **********

Table: origins
Read: http://localhost/api-practice/assessment/api/origins
Show / Read One: http://localhost/api-practice/assessment/api/origins?id=
Create: http://localhost/api-practice/assessment/api/origins/create.php
Update: http://localhost/api-practice/assessment/api/origins/update.php
Delete: http://localhost/api-practice/assessment/api/origins/delete.php

Create requirements: 'point'


********* Routes **********

Table: routes
Read: http://localhost/api-practice/assessment/api/routes
Show / Read One: http://localhost/api-practice/assessment/api/routes?id=
Create: http://localhost/api-practice/assessment/api/routes/create.php
Update: http://localhost/api-practice/assessment/api/routes/update.php
Delete: http://localhost/api-practice/assessment/api/routes/delete.php

Create requirements: 'point'

********* Destinations ********

table: destinations
Read: http://localhost/api-practice/assessment/api/destinations
Show / Read One: http://localhost/api-practice/assessment/api/destinations?id=
Create: http://localhost/api-practice/assessment/api/destinations/create.php
Update: http://localhost/api-practice/assessment/api/destinations/update.php
Delete: http://localhost/api-practice/assessment/api/destinations/delete.php?id=

Create requirements: 'point'

******** Shipping *******

Read: http://localhost/api-practice/assessment/api/shipping
Create: http://localhost/api-practice/assessment/api/shipping/create.php

Create Requirements: 'origin_id', 'destination_id', 'routes(id)'
    Note: Wrap 'routes' in "".if you want to use multiple routes, separate each route with a comma (,)


****** Origin to Routes *******

Table: route_in_origin
Create: http://localhost/api-practice/assessment/api/origins/routes/assign.php
Update: http://localhost/api-practice/assessment/api/origins/routes/update.php?id=
Delete: http://localhost/api-practice/assessment/api/origins/routes/delete.php?id=18

Create requirements: 'origin_id', 'route_id', 'cost', 'time'

***** Routes to Destination ******

Table: route_in_destination
Create: http://localhost/api-practice/assessment/api/destinations/routes/assign.php
Update: http://localhost/api-practice/assessment/api/destinations/routes/update.php?id=
Delete: http://localhost/api-practice/assessment/api/destinations/routes/delete.php?id=

Create requirements: 'destination_id', 'route_id', 'cost', 'time'