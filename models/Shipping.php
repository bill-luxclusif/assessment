<?php

class Shipping {
    private $con;
    public $table;
    public $id;

    public $destination;
    public $origin;
    public $routes;
    public $total_cost;
    public $total_time;

    public function __construct($db)
    {
        $this->con = $db;
    }

    public function read() {
        $stmt = "SELECT * FROM $this->table";
        $query = mysqli_query($this->con, $stmt);

        return $query;
    }

    public function create() {
        $stmt = "INSERT INTO $this->table(origin, destination, routes, total_cost, total_time) VALUES('$this->origin', '$this->destination', '$this->routes', '$this->total_cost', '$this->total_time')";
        $query = mysqli_query($this->con, $stmt);

        return $query;
    }

}