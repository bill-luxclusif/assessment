<?php

class RoutesInDestination {
    private $con;
    public $table;
    public $id;

    public $destination_id;
    public $route_id;
    public $cost;
    public $time_of_delivery;
    

    public function __construct($db)
    {
        $this->con = $db;
    }

    public function read() {
        $stmt = "SELECT point, cost, time_of_delivery FROM $this->table as a LEFT JOIN routes as b ON a.route_id = b.id WHERE destination_id = $this->id";
        $query = mysqli_query($this->con, $stmt);
        $routes = [];
        while($row = mysqli_fetch_assoc($query)) {
            array_push($routes, $row);
        }
        // print_r($routes);
        return $routes;
    }

    public function create() {
        $stmt = "INSERT INTO $this->table(destination_id, route_id, cost, time_of_delivery) VALUES('$this->destination_id', '$this->route_id', '$this->cost', '$this->time_of_delivery')";
        $query = mysqli_query($this->con, $stmt);

        return $query;
    }

    public function update() {
        if ($this->cost !== null) {
            $stmt = "UPDATE $this->table SET cost = '$this->cost' WHERE id = $this->id";
            $query = mysqli_query($this->con, $stmt);
        }
        if ($this->time_of_delivery != '') {
            $stmt = "UPDATE $this->table SET time_of_delivery = '$this->time_of_delivery' WHERE id = $this->id";
            $query = mysqli_query($this->con, $stmt);
        }
        return $query;
    }

    public function delete() {
        $stmt = "DELETE FROM $this->table WHERE id = $this->id";
        $query = mysqli_query($this->con, $stmt);

        return $query;
    }

    public function show() {
        $stmt = "SELECT * FROM $this->table WHERE destination_id = $this->destination_id AND route_id = $this->route_id";
        $query = mysqli_query($this->con, $stmt);
        
        return $query;
    }

}

