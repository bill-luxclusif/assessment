<?php

class Destination {
    
    private $con;
    private $table;
    private $id;

    public function __construct($db) {
        $this->con = $db;
    }

    public function read() {
        $stmt = "SELECT * FROM destinations";
        $query = mysqli_query($this->con, $stmt);

        return $query;
    }

}