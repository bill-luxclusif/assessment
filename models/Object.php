<?php

class Object {
    
    private $con;
    public $table;
    public $point_type;

    public $id;
    public $point;
    public $cost;
    public $time;

    public function __construct($db) {
        $this->con = $db;
    }

    public function read() {
        $stmt = "SELECT * FROM $this->table";
        $query = mysqli_query($this->con, $stmt);

        return $query;
    }

    public function show() {
        $stmt = "SELECT * FROM $this->table WHERE id = $this->id";
        $query = mysqli_query($this->con, $stmt);

        return $query;
    }

    public function create() {
        $stmt = "INSERT INTO $this->table(point) VALUES('$this->point')";
        
        $query = mysqli_query($this->con, $stmt);
        return $query;
    } 

    public function update() {
        if ($this->point_type == 'routes') {
            $stmt = "UPDATE $this->table SET point = '$this->point', cost = '$this->cost', time_of_delivery = '$this->time' WHERE id = $this->id";
        }
        else {
            $stmt = "UPDATE $this->table SET point = '$this->point' WHERE id = $this->id";
        }
        
        $query = mysqli_query($this->con, $stmt);
        return $query;
    }

    public function delete() {
        $stmt = "DELETE FROM $this->table WHERE id = $this->id";
        $query = mysqli_query($this->con, $stmt);

        return $query;
    }

}