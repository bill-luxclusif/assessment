<?php

$con = mysqli_connect('localhost', 'root', '', 'assessment');

include_once '../../models/Object.php';

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    $route = new Object($con);
    $route->table = 'routes';

    if (isset($_GET['id'])) {
        $route->id = $_GET['id'];

        $data = $route->show();
        $count = mysqli_num_rows($data);
        if ($count > 0) {
            $row = mysqli_fetch_assoc($data);
            extract($row);

            $data_arr = [
                'id' => $id,
                'point' => $point
            ];

            echo json_encode($data_arr);
        }
        else {
            echo json_encode(['message' => 'No route matches the id you have given']);
        }
    } 
    
    else {
        $data = $route->read();
        $count = mysqli_num_rows($data);
            
        if ($count > 0) { 
            $routes_arr = [];
            while($row = mysqli_fetch_assoc($data)) {
                array_push($routes_arr, $row);
            }
            echo json_encode($routes_arr);
        }
        else {
            echo json_encode(['message' => 'No records found']);
        }    
    }
} 
else {
    echo json_encode(['message' => 'Unauthorized Request']);
}
