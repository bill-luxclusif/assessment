<?php

include_once '../../models/Object.php';

$con = mysqli_connect('localhost', 'root', '', 'assessment');

include_once '../../models/Object.php';

if ($_SERVER['REQUEST_METHOD'] == 'DELETE') {
    $route = new Object($con);
    $route->table = 'routes';

    $data = json_decode(file_get_contents('php://input'));

    $route->id = $_GET['id'];

    if ($route->delete()) {
        echo json_encode(['message' => 'Route deleted.']);
    }
    else {
        echo json_encode((['message' => 'Route not deleted.']));
    }
}
else {
    echo json_encode(['message' => 'Unauthorized Request']);
}