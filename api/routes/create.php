<?php

include_once '../../models/Object.php';

$con = mysqli_connect('localhost', 'root', '', 'assessment');

include_once '../../models/Object.php';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $route = new Object($con);
    $route->table = 'routes';

    $data = json_decode(file_get_contents('php://input'));

    $route->point = $data->point;


    if ($route->create()) {
        echo json_encode(['message' => 'Route created.']);
    }
    else {
        echo json_encode(([
            'point' => $route->point,
            'cost' => $route->cost,
            'time' => $route->time,
            'type' => $route->type,
            'message' => 'Route not created.'
            ]));
    }
}
else {
    echo json_encode(['message' => 'Unauthorized Request']);
}