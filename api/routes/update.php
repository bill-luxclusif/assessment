<?php

$con = mysqli_connect('localhost', 'root', '', 'assessment');

include_once '../../models/Object.php';
if ($_SERVER['REQUEST_METHOD'] == 'PATCH') {
    $route = new Object($con);
    $route->table = 'routes';
    $route->point_type = 'routes';

    $data = json_decode(file_get_contents('php://input'));

    $route->id = $data->id;
    $route->point = $data->point;
    $route->cost = $data->cost;
    $route->time = $data->time;

    $route->point = $data->point;
    if ($route->update()) {
        echo json_encode(['message' => 'Route Updated.']);
    }
    else {
        echo json_encode((['message' => 'Route not updated.']));
    }
} 
else {
    echo json_encode(['message' => 'Unauthorized Request']);
}
