<?php

$con = mysqli_connect('localhost', 'root', '', 'assessment');

include_once '../../models/Object.php';

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    $routes = new Object($con);
    $destination->table = 'routes';

    $data = $destination->read();
    $count = mysqli_num_rows($data);
    if ($count > 0) {
        $routes_arr = [];

        while($row = mysqli_fetch_assoc($data)) {
            array_push($routes_arr, $row);
        }
        echo json_encode($routes_arr);        
    }
    else {
        echo json_encode(['message' => 'No records found']);
    }
} 
else {
    echo json_encode(['message' => 'Unauthorized Request']);
}
