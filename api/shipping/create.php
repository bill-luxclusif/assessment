<?php

include_once '../../models/Shipping.php';

$con = mysqli_connect('localhost', 'root', '', 'assessment');

include_once '../../models/Object.php';
include_once '../../models/RoutesInOrigin.php';
include_once '../../models/RoutesInDestination.php';
include_once '../../models/Shipping.php';

// route point to route point will always cost a fixed charge of $150 and time of 60m

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $shipping = new Shipping($con);
    $shipping->table = 'shipping';

    $routes = '';

    $data = json_decode(file_get_contents('php://input'));

    $given_routes = explode(",", $data->routes);
    $count = count($given_routes);
    
    // get point name of each given route
    foreach ($given_routes as $val) {
        $get_route_point = new Object($con);
        $get_route_point->table = 'routes';
        $get_route_point->id = $val;

        $route_data = $get_route_point->show();
        $point = mysqli_fetch_assoc($route_data);
        
        $routes .= $point['point'] .', ';
    }

    // get the cost and time from origin to route
    $get_origin = new RoutesInOrigin($con);
    $get_origin->table = 'route_in_origin';
    $get_origin->origin_id = $data->origin_id;
    $get_origin->route_id = $given_routes[0];

    $origin_data = $get_origin->show();
    $var_name1 = mysqli_fetch_assoc($origin_data);

    // get the cost and time from route to destination
    $get_destination = new RoutesInDestination($con);
    $get_destination->table = 'route_in_destination';
    $get_destination->destination_id = $data->destination_id;
    $get_destination->route_id = end($given_routes);

    $destination_data = $get_destination->show();
    $var_name2 = mysqli_fetch_assoc($destination_data);

    // assigning to shipping object
    $shipping->origin = $data->origin_id;
    $shipping->destination = $data->destination_id;
    $shipping->routes = $routes;

    $shipping->total_cost = ($count * 150) + $var_name1['cost'] + $var_name2['cost'];
    $shipping->total_time = ($count * 60) + $var_name1['time_of_delivery'] + $var_name2['time_of_delivery'];
    
    // echo json_encode($var_name2['time_of_delivery']);
    // create
    if ($shipping->create()) {
        echo json_encode(['message' => 'Shipping created.']);
    }
    else {
        echo json_encode([
            'message' => 'Shipping not created.'
        ]);
    }
}
else {
    echo json_encode(['message' => 'Unauthorized Request']);
}