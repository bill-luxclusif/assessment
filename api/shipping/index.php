<?php

include_once '../../models/Shipping.php';

$con = mysqli_connect('localhost', 'root', '', 'assessment');

include_once '../../models/Object.php';
include_once '../../models/Shipping.php';

// route point to route point will always cost a fixed charge of $150 and time of 60m

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    $shipping = new Shipping($con);
    $shipping->table = 'shipping';
    
    $data = [];

    $get_shipping = $shipping->read();
    while($row = mysqli_fetch_assoc($get_shipping)) {
        extract($row);
        
        // get origin point
        $get_origin = new Object($con);
        $get_origin->table = 'origins';
        $get_origin->id = $origin;

        $origin_data = $get_origin->show();
        $origin_res = mysqli_fetch_assoc($origin_data);

        // get destination point

        $get_destination = new Object($con);
        $get_destination->table = 'destinations';
        $get_destination->id = $destination;

        $destination_data = $get_destination->show();
        $destination_res = mysqli_fetch_assoc($destination_data);

        $shipping_details = 'Origin: '. $origin_res['point'] .' | Destination: '. $destination_res['point'] .' | Routes: '. $routes .' | Total Cost: $'. $total_cost .' | Delivery Time: '. $total_time .' minutes';

        array_push($data, $shipping_details);
    }
    echo json_encode($data);
    
    
}
else {
    echo json_encode(['message' => 'Unauthorized Request']);
}