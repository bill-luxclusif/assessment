<?php

include_once '../../models/Object.php';

$con = mysqli_connect('localhost', 'root', '', 'assessment');

include_once '../../models/Object.php';

if ($_SERVER['REQUEST_METHOD'] == 'DELETE') {
    $destination = new Object($con);
    $destination->table = 'destinations';

    $data = json_decode(file_get_contents('php://input'));

    $destination->id = $_GET['id'];

    if ($destination->delete()) {
        echo json_encode(['message' => 'Destination deleted.']);
    }
    else {
        echo json_encode((['message' => 'Destination not deleted.']));
    }
}
else {
    echo json_encode(['message' => 'Unauthorized Request']);
}