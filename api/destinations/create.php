<?php

include_once '../../models/Object.php';

$con = mysqli_connect('localhost', 'root', '', 'assessment');

include_once '../../models/Object.php';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $destination = new Object($con);
    $destination->table = 'destinations';
    

    $data = json_decode(file_get_contents('php://input'));

    $destination->point = $data->point;

    if ($destination->create()) {
        echo json_encode(['message' => 'Destination created.']);
    }
    else {
        echo json_encode((['message' => 'Destination not created.']));
    }
}
else {
    echo json_encode(['message' => 'Unauthorized Request']);
}