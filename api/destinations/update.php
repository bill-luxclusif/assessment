<?php

$con = mysqli_connect('localhost', 'root', '', 'assessment');

include_once '../../models/Object.php';
if ($_SERVER['REQUEST_METHOD'] == 'PATCH') {
    $destination = new Object($con);
    $destination->table = 'destinations';
    $destination->point_type = 'destinations';

    $data = json_decode(file_get_contents('php://input'));

    $destination->id = $data->id;

    $destination->point = $data->point; 
    if ($destination->update()) {
        echo json_encode(['message' => 'Destination Updated.']);
    }
    else {
        echo json_encode((['message' => 'Destination not updated.']));
    }
} 
else {
    echo json_encode(['message' => 'Unauthorized Request']);
}
