<?php

require '../../../models/Object.php';
require '../../../models/RoutesInDestination.php';

$con = mysqli_connect('localhost', 'root', '', 'assessment');

if ($_SERVER['REQUEST_METHOD'] == 'DELETE') {
    $route_in_destination = new RoutesInDestination($con);
    $route_in_destination->table = 'route_in_destination';

    // $data = json_decode(file_get_contents('php://input'));
    
    $route_in_destination->id = $_GET['id'];;

    if ($route_in_destination->delete()) {
        echo json_encode(['message' => 'Route deleted.']);
    }
    else {
        echo json_encode(['message' => 'Route not deleted.']);
    }
    
}
else {
    echo json_encode(['message' => 'Unauthorized Request']);
}