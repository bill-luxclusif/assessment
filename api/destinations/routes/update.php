<?php

require '../../../models/Object.php';
require '../../../models/RoutesInDestination.php';

$con = mysqli_connect('localhost', 'root', '', 'assessment');

if ($_SERVER['REQUEST_METHOD'] == 'PATCH') {
    $route_in_destination = new RoutesInDestination($con);
    $route_in_destination->table = 'route_in_destination';

    $data = json_decode(file_get_contents('php://input'));
    

    $route_in_destination->id = $_GET['id'];
    $route_in_destination->cost = $data->cost;
    $route_in_destination->time_of_delivery = $data->time;

    if ($route_in_destination->update()) {
        echo json_encode(['message' => 'Route updated.']);
    }
    else {
        echo json_encode(['message' => 'Route not updated.']);
    }
}

else {
    echo json_encode(['message' => 'Unauthorized Request']);
}