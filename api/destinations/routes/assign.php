<?php

require '../../../models/Object.php';
require '../../../models/RoutesInDestination.php';

$con = mysqli_connect('localhost', 'root', '', 'assessment');

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $route_in_destination = new RoutesInDestination($con);
    $route_in_destination->table = 'route_in_destination';

    $data = json_decode(file_get_contents('php://input'));
    

    $route_in_destination->destination_id = $data->destination_id;
    $route_in_destination->route_id = $data->route_id;
    $route_in_destination->cost = $data->cost;
    $route_in_destination->time_of_delivery = $data->time;

    if ($route_in_destination->create()) {
        echo json_encode(['message' => 'Route assigned to point.']);
    }
    else {
        echo json_encode([
            'message' => 'Route not assigned to point.'
        ]);
    }
}

else {
    echo json_encode(['message' => 'Unauthorized Request']);
}