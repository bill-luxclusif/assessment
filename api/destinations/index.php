<?php

$con = mysqli_connect('localhost', 'root', '', 'assessment');

include_once '../../models/Object.php';
include_once '../../models/RoutesInDestination.php';

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    $destination = new Object($con);
    $destination->table = 'destinations';

    // checks if an id is given 
    if (isset($_GET['id'])) {
        $destination->id = $_GET['id'];
 
        // get the data of the given destination id
        $data = $destination->show();
        $count = mysqli_num_rows($data);
        if ($count > 0) {
            $row = mysqli_fetch_assoc($data);
            extract($row);

            $data_arr = [];

            $data_arr['data'] = [
                'id' => $id,
                'point' => $point
            ];
            $data_arr['routes'] = [];

            // gets the available route for this destination
            $available_routes = new RoutesInDestination($con);
            $available_routes->table = 'route_in_destination';
            $available_routes->id = $_GET['id'];

            $data_arr['routes'] = $available_routes->read();
        
            echo json_encode($data_arr);
        }
        else {
            echo json_encode(['message' => 'No destination matches the id you have given']);
        }
    } 
    
    else {
        $data = $destination->read();
        $count = mysqli_num_rows($data);
            
        if ($count > 0) { 
            $destinations_arr = [];
            while($row = mysqli_fetch_assoc($data)) {
                array_push($destinations_arr, $row);
            }
            echo json_encode($destinations_arr);
        }
        else {
            echo json_encode(['message' => 'No records found']);
        }    
    }
} 
else {
    echo json_encode(['message' => 'Unauthorized Request']);
}
