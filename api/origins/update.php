<?php

$con = mysqli_connect('localhost', 'root', '', 'assessment');

include_once '../../models/Object.php';
if ($_SERVER['REQUEST_METHOD'] == 'PATCH') {
    $origin = new Object($con);
    $origin->table = 'origins';
    $origin->point_type = 'origins';

    $data = json_decode(file_get_contents('php://input'));

    $origin->id = $data->id;

    $origin->point = $data->point;
    if ($origin->update()) {
        echo json_encode(['message' => 'Origin Updated.']);
    }
    else {
        echo json_encode((['message' => 'Origin not updated.']));
    }
} 
else {
    echo json_encode(['message' => 'Unauthorized Request']);
}
