<?php

include_once '../../models/Object.php';

$con = mysqli_connect('localhost', 'root', '', 'assessment');

include_once '../../models/Object.php';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $origin = new Object($con);
    $origin->table = 'origins';
    $origin->point_type = 'origins';

    $data = json_decode(file_get_contents('php://input'));

    $origin->point = $data->point;

    if ($origin->create()) {
        echo json_encode(['message' => 'Origin created.']);
    }
    else {
        echo json_encode((['message' => 'Origin not created.']));
    }
}
else {
    echo json_encode(['message' => 'Unauthorized Request']);
}