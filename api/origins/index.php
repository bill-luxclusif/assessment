<?php

$con = mysqli_connect('localhost', 'root', '', 'assessment');

include_once '../../models/Object.php';
include_once '../../models/RoutesInOrigin.php';

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    $origin = new Object($con);
    $origin->table = 'origins';

    if (isset($_GET['id'])) {
        $origin->id = $_GET['id'];

        // get the data of the given origin id
        $data = $origin->show();
        $count = mysqli_num_rows($data);
        if ($count > 0) {
            $row = mysqli_fetch_assoc($data);
            extract($row);

            $data_arr = [];

            $data_arr = [
                'id' => $id,
                'point' => $point
            ];

            $data_arr['routes'] = [];

            // gets the available route for this destination
            $available_routes = new RoutesInOrigin($con);
            $available_routes->table = 'route_in_origin';
            $available_routes->id = $_GET['id'];

            $data_arr['routes'] = $available_routes->read();

            echo json_encode($data_arr);
        }
        else {
            echo json_encode(['message' => 'No origin matches the id you have given']);
        }
    } 
    
    else {
        $data = $origin->read();
        $count = mysqli_num_rows($data);
            
        if ($count > 0) { 
            $origins_arr = [];
            while($row = mysqli_fetch_assoc($data)) {
                array_push($origins_arr, $row);
            }
            echo json_encode($origins_arr);
        }
        else {
            echo json_encode(['message' => 'No records found']);
        }    
    }
} 
else {
    echo json_encode(['message' => 'Unauthorized Request']);
}
