<?php

include_once '../../models/Object.php';

$con = mysqli_connect('localhost', 'root', '', 'assessment');

include_once '../../models/Object.php';

if ($_SERVER['REQUEST_METHOD'] == 'DELETE') {
    $origin = new Object($con);
    $origin->table = 'origins';

    $data = json_decode(file_get_contents('php://input'));

    $origin->id = $_GET['id'];

    if ($origin->delete()) {
        echo json_encode(['message' => 'Origins deleted.']);
    }
    else {
        echo json_encode((['message' => 'Origins not deleted.']));
    }
}
else {
    echo json_encode(['message' => 'Unauthorized Request']);
}