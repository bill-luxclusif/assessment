<?php

require '../../../models/Object.php';
require '../../../models/RoutesInOrigin.php';

$con = mysqli_connect('localhost', 'root', '', 'assessment');

if ($_SERVER['REQUEST_METHOD'] == 'DELETE') {
    $route_in_origin = new RoutesInOrigin($con);
    $route_in_origin->table = 'route_in_origin';

    // $data = json_decode(file_get_contents('php://input'));
    
    $route_in_origin->id = $_GET['id'];

    if ($route_in_origin->delete()) {
        echo json_encode(['message' => 'Route deleted.']);
    }
    else {
        echo json_encode(['message' => 'Route not deleted.']);
    }
    
}
else {
    echo json_encode(['message' => 'Unauthorized Request']);
}