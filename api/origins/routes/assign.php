<?php

require '../../../models/Object.php';
require '../../../models/RoutesInOrigin.php';

$con = mysqli_connect('localhost', 'root', '', 'assessment');

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $route_in_origin = new RoutesInOrigin($con);
    $route_in_origin->table = 'route_in_origin';

    $data = json_decode(file_get_contents('php://input'));
    

    $route_in_origin->origin_id = $data->origin_id;
    $route_in_origin->route_id = $data->route_id;
    $route_in_origin->cost = $data->cost;
    $route_in_origin->time_of_delivery = $data->time;

    if ($route_in_origin->create()) {
        echo json_encode(['message' => 'Route assigned to point.']);
    }
    else {
        echo json_encode(['message' => 'Route not assigned to point.']);
    }
}

else {
    echo json_encode(['message' => 'Unauthorized Request']);
}