<?php

require '../../../models/Object.php';
require '../../../models/RoutesInOrigin.php';

$con = mysqli_connect('localhost', 'root', '', 'assessment');

if ($_SERVER['REQUEST_METHOD'] == 'PATCH') {
    $route_in_origin = new RoutesInOrigin($con);
    $route_in_origin->table = 'route_in_origin';

    $data = json_decode(file_get_contents('php://input'));
    

    $route_in_origin->id = $_GET['id'];
    $route_in_origin->cost = $data->cost;
    $route_in_origin->time_of_delivery = $data->time;

    if ($route_in_origin->update()) {
        echo json_encode(['message' => 'Route updated.']);
    }
    else {
        echo json_encode(['message' => 'Route not updated.']);
    }
}

else {
    echo json_encode(['message' => 'Unauthorized Request']);
}